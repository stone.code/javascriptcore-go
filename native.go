// Copyright 2011 Robert W Johnstone. All rights reserved.
// Use of this source code is governed by a BSD-style
// license that can be found in the LICENSE file.

package javascriptcore

// #include <stdlib.h>
// #include <JavaScriptCore/JSStringRef.h>
// #include <JavaScriptCore/JSObjectRef.h>
// #include "callback.h"
import "C"
import "reflect"
import "syscall"
import "unsafe"

type object_data struct {
	typ    reflect.Type
	val    reflect.Value
	method int
}

var (
	nativecallback C.JSClassRef
	nativefunction C.JSClassRef
	nativeobject   C.JSClassRef
	nativemethod   C.JSClassRef
	objects        map[uintptr]*object_data
)

func init() {
	// Create the class definition for JavaScriptCore
	nativecallback = C.JSClassDefinition_NativeCallback()
	if nativecallback == nil {
		panic(syscall.ENOMEM)
	}

	// Create the class definition for JavaScriptCore
	nativeobject = C.JSClassDefinition_NativeObject()
	if nativeobject == nil {
		panic(syscall.ENOMEM)
	}

	// Create the class definition for JavaScriptCore
	nativefunction = C.JSClassDefinition_NativeFunction()
	if nativefunction == nil {
		panic(syscall.ENOMEM)
	}

	// Create the class definition for JavaScriptCore
	nativemethod = C.JSClassDefinition_NativeMethod()
	if nativemethod == nil {
		panic(syscall.ENOMEM)
	}

	// Create map for native objects
	objects = make(map[uintptr]*object_data)
}

func javascript_to_reflect(ctx *Context, param []*Value) []reflect.Value {
	ret := make([]reflect.Value, len(param))

	for index, item := range param {
		var goval interface{}

		switch ctx.Kind(item) {
		case TypeBoolean:
			goval = ctx.ToBoolean(item)
		case TypeNumber:
			goval = ctx.ToNumberOrDie(item)
		case TypeString:
			goval = ctx.ToStringOrDie(item)
		default:
			panic("Parameter can not be converted to Go native type.")
		}

		ret[index] = reflect.ValueOf(goval)
	}

	return ret
}

// Takes variables passed from CGO that point to an array of javascript values
// and creates a slice in Go that points to that same array.  In addition to
// handling type conversion, this function also fixes up the slice capacity.
func javascript_to_array(arguments unsafe.Pointer, argumentCount uint) []*Value {
	ret := (*[1 << 14]*Value)(arguments)[0:argumentCount]
	(*reflect.SliceHeader)(unsafe.Pointer(&ret)).Cap = int(argumentCount)
	return ret
}

//=========================================================
// Finalizer from JavaScriptCore for all native objects
//---------------------------------------------------------

func register(data *object_data) {
	id := uintptr(unsafe.Pointer(data))
	objects[id] = data
}

//export finalize_go
func finalize_go(data unsafe.Pointer) {
	// Called from JavaScriptCore finalizer methods
	id := uintptr(data)
	delete(objects, id)
}

//=========================================================
// Native Callback
//---------------------------------------------------------

// A GoFunctionCallback is a function or closure that can be used to create a
// generic JavaScript function.  See NewFunctionWithCallback.
type GoFunctionCallback func(ctx *Context, obj *Object, thisObject *Object, arguments []*Value) (ret *Value)

// NewFunctionWithCallback creates a new JavaScript function object that can be
// called from within the JavaScript context.  The JavaScript function's 
// behaviour is implemented using a generic callback inside of Go.
//
// Unlike wraping native methods directly, a JavaScript function created using 
// a callback is capable of handling any mixture of valid JavaScript values as
// parameters, and can exhibit polymorphic behaviour.
//
// The callback can panic.  If the callback panics, the value thrown is converted
// using the method NewError using the Context.  The resulting JavaScript value
// is then thrown within the JavaScript context.
//
// See also NewFunctionWithNative.
func (ctx *Context) NewFunctionWithCallback(callback GoFunctionCallback) *Object {
	// Register the native Go object
	data := &object_data{
		reflect.TypeOf(callback),
		reflect.ValueOf(callback),
		0}
	register(data)

	ret := C.JSObjectMake(ctx.ptr(), nativecallback, unsafe.Pointer(data))
	return toObject(ret)
}

//export nativecallback_CallAsFunction_go
func nativecallback_CallAsFunction_go(data_ptr unsafe.Pointer, ctx unsafe.Pointer, obj unsafe.Pointer, thisObject unsafe.Pointer, argumentCount uint, arguments unsafe.Pointer, exception *unsafe.Pointer) unsafe.Pointer {
	defer func() {
		if r := recover(); r != nil {
			val, _ := (*Context)(ctx).NewError(r)
			*exception = unsafe.Pointer(val)
		}
	}()

	data := (*object_data)(data_ptr)
	ret := data.val.Interface().(GoFunctionCallback)(
		(*Context)(ctx), (*Object)(obj), (*Object)(thisObject),
		javascript_to_array(arguments, argumentCount))
	return unsafe.Pointer(ret)
}

//=========================================================
// Native Function
//---------------------------------------------------------

// NewFunctionWithNative creates a new JavaScript function object that can be 
// called from within the JavaScript context.  The JavaScript function's 
// behaviour is implemented by wrapping a native Go function or closure,
// and coercing JavaScript values as required.
//
// For compatibility with JavaScript, the function or closure must return 
// either zero or one parameters.  Any additional return parameters will be 
// ignored when returning the results to JavaScript.  See the method NewValue of
// the type Context for information about type conversions for the returned
// value.
//
// The function can panic.  If the function panics, the value throw is converted
// using the method NewError using the Context.  The resulting JavaScript value
// is then thrown within the JavaScript context.
//
// See also NewFunctionWithCallback.
func (ctx *Context) NewFunctionWithNative(fn interface{}) *Object {
	// Sanity checks on the function
	if typ := reflect.TypeOf(fn); typ.Kind() == reflect.Func && typ.NumOut() > 1 {
		panic("Bad native function:  too many output parameters")
	}

	// Create Go-side registration
	data := &object_data{
		reflect.TypeOf(fn),
		reflect.ValueOf(fn),
		0}
	register(data)

	ret := C.JSObjectMake(ctx.ptr(), nativefunction, unsafe.Pointer(data))
	return toObject(ret)
}

func docall(ctx *Context, val reflect.Value, argumentCount uint, arguments unsafe.Pointer) *Value {
	// Step one, convert the JavaScriptCore array of arguments to 
	// an array of reflect.Values.  
	var in []reflect.Value
	if argumentCount != 0 {
		in = javascript_to_reflect((*Context)(ctx), javascript_to_array(arguments, argumentCount))
	}

	// Step two, perform the call
	out := val.Call(in)

	// Step three, convert the function return value back to JavaScriptCore
	if len(out) == 0 {
		return nil
	}
	// len(out) should be equal to 1
	return ctx.NewValue(out[0].Interface())
}

//export nativefunction_CallAsFunction_go
func nativefunction_CallAsFunction_go(data_ptr unsafe.Pointer, ctx unsafe.Pointer, _ unsafe.Pointer, _ unsafe.Pointer, argumentCount uint, arguments unsafe.Pointer, exception *unsafe.Pointer) unsafe.Pointer {
	defer func() {
		if r := recover(); r != nil {
			val, _ := (*Context)(ctx).NewError(r)
			*exception = unsafe.Pointer(val)
		}
	}()

	// recover the object
	data := (*object_data)(data_ptr)
	ret := docall((*Context)(ctx), data.val, argumentCount, arguments)
	return unsafe.Pointer(ret)
}

//=========================================================
// Native Object
//---------------------------------------------------------

// NewNativeObject creates a new JavaScript object that wraps a native Go
// structure or a pointer to a structure.  The structures fields and methods
// will be accessible from within the JavaScript context.  If wrapping a
// pointer to a structure, then changes made in JavaScript will be visible
// in Go.
//
// If the argument is a pointer to a structure, than updates to the structure's
// fields within JavaScript will also be visible within Go.
//
// The structure's methods can be called from from within JavaScript.  The 
// methods act according to the same rules as NewFunctionWithNative.
func (ctx *Context) NewNativeObject(obj interface{}) *Object {
	typ := reflect.TypeOf(obj)
	if typ.Kind() != reflect.Struct && !(typ.Kind() == reflect.Ptr && typ.Elem().Kind() == reflect.Struct) {
		panic("NewNativeObject only accepts structures and pointers to structures")
	}

	data := &object_data{
		reflect.TypeOf(obj),
		reflect.ValueOf(obj),
		0}
	register(data)

	ret := C.JSObjectMake(ctx.ptr(), nativeobject, unsafe.Pointer(data))
	return toObject(ret)
}

//export nativeobject_GetProperty_go
func nativeobject_GetProperty_go(data_ptr, ctx, _, propertyName unsafe.Pointer, exception *unsafe.Pointer) unsafe.Pointer {
	// Get name of property as a go string
	name := (*String)(propertyName).String()

	// Reconstruct the object interface
	data := (*object_data)(data_ptr)

	// Drill down through reflect to find the property
	val := data.val
	if val.Kind() == reflect.Ptr {
		val = val.Elem()
	}
	if val.Kind() != reflect.Struct {
		return nil
	}

	// Can we locate a field with the proper name?
	field := val.FieldByName(name)
	if field.IsValid() {
		return unsafe.Pointer((*Context)(ctx).NewValue(field.Interface()))
	}

	// Can we locate a method with the proper name?
	typ := data.typ
	if method, ok := typ.MethodByName(name); ok {
		ret := newNativeMethod((*Context)(ctx), data, method.Index)
		return unsafe.Pointer(ret)
	}

	// No matches found
	return nil
}

func internal_go_error(ctx *Context) *Value {
	param := ctx.NewString("Internal Go error.")

	exception := C.JSValueRef(nil)
	ret := C.JSObjectMakeError(ctx.ptr(),
		C.size_t(1), (*C.JSValueRef)(unsafe.Pointer(&param)),
		&exception)
	if ret != nil {
		return (*Value)(unsafe.Pointer(ret))
	}
	if exception != nil {
		return toValue(exception)
	}
	panic("Internal Go error.")
}

//export nativeobject_SetProperty_go
func nativeobject_SetProperty_go(data_ptr, ctx, _, propertyName, value unsafe.Pointer, exception *unsafe.Pointer) C.char {
	// Get name of property as a go string
	name := (*String)(propertyName).String()

	// Reconstruct the object interface
	data := (*object_data)(data_ptr)

	// Drill down through reflect to find the property
	val := data.val
	if val.Kind() == reflect.Ptr {
		val = val.Elem()
	}
	if val.Kind() != reflect.Struct {
		*exception = unsafe.Pointer(internal_go_error((*Context)(ctx)))
		return 1
	}

	field := val.FieldByName(name)
	if !field.IsValid() {
		return 0
	}

	switch field.Kind() {
	case reflect.String:
		str, err := (*Context)(ctx).ToString((*Value)(value))
		if err == nil {
			field.SetString(str)
		} else {
			*exception = unsafe.Pointer(err)
		}

	case reflect.Float32, reflect.Float64:
		flt, err := (*Context)(ctx).ToNumber((*Value)(value))
		if err == nil {
			field.SetFloat(flt)
		} else {
			*exception = unsafe.Pointer(err)
		}

	case reflect.Int, reflect.Int8, reflect.Int16, reflect.Int32, reflect.Int64:
		flt, err := (*Context)(ctx).ToNumber((*Value)(value))
		if err == nil {
			field.SetInt(int64(flt))
		} else {
			*exception = unsafe.Pointer(err)
		}

	case reflect.Uint, reflect.Uint8, reflect.Uint16, reflect.Uint32, reflect.Uint64:
		flt, err := (*Context)(ctx).ToNumber((*Value)(value))
		if err == nil {
			if flt >= 0 {
				field.SetUint(uint64(flt))
			} else {
				err1, err := (*Context)(ctx).NewError("Number must be greater than or equal to zero.")
				if err == nil {
					*exception = unsafe.Pointer(err1)
				} else {
					*exception = unsafe.Pointer(err)
				}
			}
		} else {
			*exception = unsafe.Pointer(err)
		}

	default:
		panic("Parameter can not be converted to Go native type.")
	}
	return 1
}

//export nativeobject_ConvertToString_go
func nativeobject_ConvertToString_go(data_ptr, ctx, obj unsafe.Pointer) unsafe.Pointer {
	// Reconstruct the object interface
	data := (*object_data)(data_ptr)

	// Can we get a string?
	if stringer, ok := data.val.Interface().(stringer); ok {
		str := stringer.String()
		ret := NewString(str)
		return unsafe.Pointer(ret)
	}

	return nil
}

//=========================================================
// Native Method
//---------------------------------------------------------

func newNativeMethod(ctx *Context, obj *object_data, method int) *Object {
	data := &object_data{
		obj.typ,
		obj.val,
		method}
	register(data)

	ret := C.JSObjectMake(ctx.ptr(), nativemethod, unsafe.Pointer(data))
	return toObject(ret)
}

//export nativemethod_CallAsFunction_go
func nativemethod_CallAsFunction_go(data_ptr, ctx, obj, thisObject unsafe.Pointer, argumentCount uint, arguments unsafe.Pointer, exception *unsafe.Pointer) unsafe.Pointer {
	defer func() {
		if r := recover(); r != nil {
			val, _ := (*Context)(ctx).NewError(r)
			*exception = unsafe.Pointer(val)
		}
	}()

	// Reconstruct the object interface
	data := (*object_data)(data_ptr)

	// Get the method
	method := data.val.Method(data.method)

	// Perform the call
	ret := docall((*Context)(ctx), method, argumentCount, arguments)
	return unsafe.Pointer(ret)
}
