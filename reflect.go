// Copyright 2011 Robert W Johnstone. All rights reserved.
// Use of this source code is governed by a BSD-style
// license that can be found in the LICENSE file.

package javascriptcore

// #include <stdlib.h>
import "C"
import "reflect"

// NewValue creates a new JavaScript value representing the Go value.
// This functions can successfully convert all integers, unsigned integers, 
// floats, boolean, and string values into JavaScript values.  Some more
// complicated types can be wrapped.  First, this function will wrap a Go 
// function or closure into a JavaScript value that can be called from within 
// the JavaScript context.  Second, this function will wrap a structure or a
// pointer to a structure into a JavaScript value that will have the same
// properties and methods.  If wrapping a pointer to a structure, updates
// to the fields in JavaScript will be visible in Go.
func (ctx *Context) NewValue(value interface{}) *Value {
	// Handle simple case right off
	if value == nil {
		return ctx.NewNullValue()
	}

	// The interface is not already a JavaScript value.
	// Use reflection to perform a conversion.
	val := reflect.ValueOf(value)

	// Handle simple types directly.  These can be identified by their
	// types in the package 'reflect'.
	switch val.Kind() {
	case reflect.Int, reflect.Int8, reflect.Int16, reflect.Int32, reflect.Int64:
		r := val.Int()
		return ctx.NewNumberValue(float64(r))
	case reflect.Uint, reflect.Uint8, reflect.Uint16, reflect.Uint32, reflect.Uint64:
		r := val.Uint()
		return ctx.NewNumberValue(float64(r))
	case reflect.Float32, reflect.Float64:
		r := val.Float()
		return ctx.NewNumberValue(r)
	case reflect.String:
		r := val.String()
		return ctx.NewString(r)
	case reflect.Bool:
		r := val.Bool()
		return ctx.NewBooleanValue(r)
	case reflect.Func:
		r := ctx.NewFunctionWithNative(val.Interface())
		return r.ToValue()
	case reflect.Struct:
		r := ctx.NewNativeObject(val.Interface())
		return r.ToValue()
	case reflect.Ptr:
		if val.IsNil() {
			return ctx.NewNullValue()
		}
		// Allows functions to return JavaScriptCore values and objects
		// directly.  These we can return without conversion.
		if ret, ok := val.Interface().(*Value); ok {
			// Type is already a JavaScriptCore value
			return ret
		}
		if ret, ok := val.Interface().(*Object); ok {
			// Type is already a JavaScriptCore object
			// nearly there
			return ret.ToValue()
		}

		if val.Elem().Kind() == reflect.Struct {
			ret := ctx.NewNativeObject(val.Interface())
			return ret.ToValue()
		}
	}

	// No acceptable conversion found.
	panic("Parameter can not be converted to JavaScript value from Go native type.")
	return nil
}
