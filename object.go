// Copyright 2011 Robert W Johnstone. All rights reserved.
// Use of this source code is governed by a BSD-style
// license that can be found in the LICENSE file.

package javascriptcore

// #include <stdlib.h>
// #include <JavaScriptCore/JSStringRef.h>
// #include <JavaScriptCore/JSObjectRef.h>
// #include "callback.h"
import "C"
import "unsafe"

// An Object contains the state associated with a JavaScript object. There are
// very few member functions for this type.  Most operations require an
// execution context (see the type Context).
//
// An Object is a Value.  However, in Go, a value with type *Object can not be
// used in place of a value with type *Value.  To bridge this gap between the
// C API and Go, the member function ToValue is provided.
type Object struct {
	Value
}

func release_jsstringref_array(refs []C.JSStringRef) {
	for i := 0; i < len(refs); i++ {
		if refs[i] != nil {
			C.JSStringRelease(refs[i])
		}
	}
}

func (ctx *Context) NewObject() *Object {
	ret := C.JSObjectMake(ctx.ptr(), nil, nil)
	return toObject(ret)
}

func (ctx *Context) NewArray(items []*Value) (arr *Object, err *Value) {
	var exception C.JSValueRef

	ret := C.JSObjectRef(nil)
	if items != nil {
		ret = C.JSObjectMakeArray(ctx.ptr(),
			C.size_t(len(items)), (*C.JSValueRef)(unsafe.Pointer(&items[0])),
			&exception)
	} else {
		ret = C.JSObjectMakeArray(ctx.ptr(),
			0, nil,
			&exception)
	}
	if exception != nil {
		return nil, toValue(exception)
	}
	return toObject(ret), nil
}

func (ctx *Context) NewDate() (date *Object, err *Value) {
	var exception C.JSValueRef

	ret := C.JSObjectMakeDate(ctx.ptr(),
		0, nil,
		&exception)
	if exception != nil {
		return nil, toValue(exception)
	}
	return toObject(ret), nil
}

func (ctx *Context) NewDateWithMilliseconds(milliseconds float64) (date *Object, err *Value) {
	var exception C.JSValueRef

	param := ctx.NewNumberValue(milliseconds).ptr()

	ret := C.JSObjectMakeDate(ctx.ptr(),
		C.size_t(1), &param, &exception)
	if exception != nil {
		return nil, toValue(exception)
	}
	return toObject(ret), nil
}

func (ctx *Context) NewDateWithString(date string) (obj *Object, err *Value) {
	var exception C.JSValueRef

	param := ctx.NewString(date).ptr()

	ret := C.JSObjectMakeDate(ctx.ptr(),
		C.size_t(1), &param, &exception)
	if exception != nil {
		return nil, toValue(exception)
	}
	return toObject(ret), nil
}

type stringer interface {
	String() string
}

func (ctx *Context) NewError(message interface{}) (obj *Object, err *Value) {
	if str, ok := message.(error); ok {
		return ctx.NewErrorFromString(str.Error())
	}
	if str, ok := message.(stringer); ok {
		return ctx.NewErrorFromString(str.String())
	}
	if str, ok := message.(string); ok {
		return ctx.NewErrorFromString(str)
	}

	return ctx.NewErrorFromString("Unknown panic from within Go.")
}

func (ctx *Context) NewErrorFromString(message string) (obj *Object, err *Value) {
	var exception C.JSValueRef

	param := ctx.NewString(message).ptr()

	ret := C.JSObjectMakeError(ctx.ptr(),
		C.size_t(1), &param, &exception)
	if exception != nil {
		return nil, toValue(exception)
	}
	return toObject(ret), nil
}

func (ctx *Context) NewRegExp(regex string) (re *Object, err *Value) {
	var exception C.JSValueRef

	param := ctx.NewString(regex).ptr()

	ret := C.JSObjectMakeRegExp(ctx.ptr(),
		C.size_t(1), &param, &exception)
	if exception != nil {
		return nil, toValue(exception)
	}
	return toObject(ret), nil
}

func (ctx *Context) NewRegExpFromValues(parameters []*Value) (re *Object, err *Value) {
	var exception C.JSValueRef

	ret := C.JSObjectMakeRegExp(ctx.ptr(),
		C.size_t(len(parameters)), (*C.JSValueRef)(unsafe.Pointer(&parameters[0])),
		&exception)
	if exception != nil {
		return nil, toValue(exception)
	}
	return toObject(ret), nil
}

func (ctx *Context) NewFunction(name string, parameters []string, body string, source_url string, starting_line_number int) (fn *Object, err *Value) {
	Cname := NewString(name)
	defer Cname.Release()

	Cparameters := make([]C.JSStringRef, len(parameters))
	defer release_jsstringref_array(Cparameters)
	for i := 0; i < len(parameters); i++ {
		Cparameters[i] = NewString(parameters[i]).ptr()
	}

	Cbody := NewString(body)
	defer Cbody.Release()

	var sourceRef *String
	if source_url != "" {
		sourceRef = NewString(source_url)
		defer sourceRef.Release()
	}

	var exception C.JSValueRef

	ret := C.JSObjectMakeFunction(ctx.ptr(),
		Cname.ptr(),
		C.unsigned(len(Cparameters)), &Cparameters[0],
		Cbody.ptr(),
		sourceRef.ptr(),
		C.int(starting_line_number), &exception)
	if exception != nil {
		return nil, toValue(exception)
	}
	return toObject(ret), nil
}

func (ctx *Context) GetPrototype(obj *Object) *Value {
	ret := C.JSObjectGetPrototype(ctx.ptr(), obj.ptr())
	return toValue(ret)
}

func (ctx *Context) SetPrototype(obj *Object, rhs *Value) {
	C.JSObjectSetPrototype(ctx.ptr(),
		obj.ptr(), rhs.ptr())
}

func (ctx *Context) HasProperty(obj *Object, name string) bool {
	jsstr := NewString(name)
	defer jsstr.Release()

	ret := C.JSObjectHasProperty(ctx.ptr(), obj.ptr(), jsstr.ptr())
	return bool(ret)
}

func (ctx *Context) GetProperty(obj *Object, name string) (val *Value, err *Value) {
	jsstr := NewString(name)
	defer jsstr.Release()

	var exception C.JSValueRef

	ret := C.JSObjectGetProperty(ctx.ptr(), obj.ptr(), jsstr.ptr(), &exception)
	if exception != nil {
		return nil, toValue(exception)
	}

	return toValue(ret), nil
}

func (ctx *Context) GetPropertyAtIndex(obj *Object, index uint16) (val *Value, err *Value) {
	var exception C.JSValueRef

	ret := C.JSObjectGetPropertyAtIndex(ctx.ptr(), obj.ptr(), C.unsigned(index), &exception)
	if exception != nil {
		return nil, toValue(exception)
	}

	return toValue(ret), nil
}

func (ctx *Context) SetProperty(obj *Object, name string, rhs *Value, attributes uint8) (err *Value) {
	jsstr := NewString(name)
	defer jsstr.Release()

	var exception C.JSValueRef

	C.JSObjectSetProperty(ctx.ptr(), obj.ptr(), jsstr.ptr(), rhs.ptr(),
		(C.JSPropertyAttributes)(attributes), &exception)
	if exception != nil {
		return toValue(exception)
	}

	return nil
}

func (ctx *Context) SetPropertyAtIndex(obj *Object, index uint16, rhs *Value) (err *Value) {
	var exception C.JSValueRef

	C.JSObjectSetPropertyAtIndex(ctx.ptr(), obj.ptr(), C.unsigned(index),
		rhs.ptr(), &exception)
	if exception != nil {
		return toValue(exception)
	}

	return nil
}

func (ctx *Context) DeleteProperty(obj *Object, name string) (bool, *Value) {
	jsstr := NewString(name)
	defer jsstr.Release()

	var exception C.JSValueRef

	ret := C.JSObjectDeleteProperty(ctx.ptr(), obj.ptr(), jsstr.ptr(), &exception)
	if exception != nil {
		return false, toValue(exception)
	}

	return bool(ret), nil
}

// GetPrivate retrieves an unsafe pointer that has been stored with the 
// JavaScript object.
func (obj *Object) GetPrivate() unsafe.Pointer {
	ret := C.JSObjectGetPrivate(obj.ptr())
	return ret
}

// SetPrivate stores an unsafe pointer with the JavaScript object.  Note that
// the Go to JavaScript binding uses this field to associate JavaScript objects
// with native data in Go, and so users of the library should refrain from using
// this method with objects created to reflect native Go values.
func (obj *Object) SetPrivate(data unsafe.Pointer) bool {
	ret := C.JSObjectSetPrivate(obj.ptr(), data)
	return bool(ret)
}

// ToValue casts the *Object to a *Value.  Since in JavaScript every Object is
// a Value, this methods helps bridge the incompatibility between the C API and
// the Go API, where inheritence is not directly expressible.
func (obj *Object) ToValue() *Value {
	return (*Value)(unsafe.Pointer(obj))
}

func (ctx *Context) IsFunction(obj *Object) bool {
	ret := C.JSObjectIsFunction(ctx.ptr(), obj.ptr())
	return bool(ret)
}

func (ctx *Context) CallAsFunction(obj *Object, thisObject *Object, parameters []*Value) (val *Value, err *Value) {
	var exception C.JSValueRef

	var Cparameters *C.JSValueRef
	if len(parameters) > 0 {
		Cparameters = (*C.JSValueRef)(unsafe.Pointer(&parameters[0]))
	}

	ret := C.JSObjectCallAsFunction(ctx.ptr(),
		obj.ptr(),
		thisObject.ptr(),
		C.size_t(len(parameters)),
		Cparameters,
		&exception)
	if exception != nil {
		return nil, toValue(exception)
	}

	return toValue(ret), nil
}

func (ctx *Context) IsConstructor(obj *Object) bool {
	ret := C.JSObjectIsConstructor(ctx.ptr(), obj.ptr())
	return bool(ret)
}

func (ctx *Context) CallAsConstructor(obj *Object, parameters []*Value) (val *Object, err *Value) {
	var exception C.JSValueRef

	var Cparameters *C.JSValueRef
	if len(parameters) > 0 {
		Cparameters = (*C.JSValueRef)(unsafe.Pointer(&parameters[0]))
	}

	ret := C.JSObjectCallAsConstructor(ctx.ptr(),
		obj.ptr(),
		C.size_t(len(parameters)),
		Cparameters,
		&exception)
	if exception != nil {
		return nil, toValue(exception)
	}

	return toObject(ret), nil
}

//=========================================================
// PropertyNameArray
//

const (
	propertyAttributeNone       = 0
	propertyAttributeReadOnly   = 1 << 1
	propertyAttributeDontEnum   = 1 << 2
	propertyAttributeDontDelete = 1 << 3
)

const (
	classAttributeNone                 = 0
	classAttributeNoAutomaticPrototype = 1 << 1
)

type PropertyNameArray struct {
	private int // Internal structure is never accessed by Go.
}

func (ctx *Context) CopyPropertyNames(obj *Object) *PropertyNameArray {
	ret := C.JSObjectCopyPropertyNames(ctx.ptr(), obj.ptr())
	return (*PropertyNameArray)(unsafe.Pointer(ret))
}

func (ref *PropertyNameArray) Retain() {
	C.JSPropertyNameArrayRetain(C.JSPropertyNameArrayRef(unsafe.Pointer(ref)))
}

func (ref *PropertyNameArray) Release() {
	C.JSPropertyNameArrayRelease(C.JSPropertyNameArrayRef(unsafe.Pointer(ref)))
}

func (ref *PropertyNameArray) Count() uint16 {
	ret := C.JSPropertyNameArrayGetCount(C.JSPropertyNameArrayRef(unsafe.Pointer(ref)))
	return uint16(ret)
}

func (ref *PropertyNameArray) NameAtIndex(index uint16) string {
	jsstr := C.JSPropertyNameArrayGetNameAtIndex(C.JSPropertyNameArrayRef(unsafe.Pointer(ref)), C.size_t(index))
	defer C.JSStringRelease(jsstr)
	return (*String)(unsafe.Pointer(jsstr)).String()
}
