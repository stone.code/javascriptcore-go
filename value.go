// Copyright 2011 Robert W Johnstone. All rights reserved.
// Use of this source code is governed by a BSD-style
// license that can be found in the LICENSE file.

package javascriptcore

// #include <stdlib.h>
// #include <JavaScriptCore/JSStringRef.h>
// #include <JavaScriptCore/JSValueRef.h>
import "C"
import "unsafe"

// An Value contains the state associated with a JavaScript value.
// Because all operations on a value occur within a Context, the functions for
// manipulating variables of type Value are all methods of Context objects.
type Value struct {
	private int // Internal structure is never accessed by Go.
}

// A  Kind describes the type of a JavaScript value.  It is the same as type
// of a JavaScript value, but renamed to match the naming convention of the
// reflect package in Go.
type Kind int

const (
	TypeUndefined = Kind(0)    // The unique undefined value.
	TypeNull      = Kind(iota) // The unique null value.
	TypeBoolean   = Kind(iota) // The value contains a boolean.
	TypeNumber    = Kind(iota) // The value contains a number.
	TypeString    = Kind(iota) // The value contains a string.
	TypeObject    = Kind(iota) // The value contains an object.
)

// String return a string describing the kind.
func (k Kind) String() string {
	switch k {
	case TypeUndefined:
		return "undefined"
	case TypeNull:
		return "null"
	case TypeBoolean:
		return "boolean"
	case TypeNumber:
		return "number"
	case TypeString:
		return "string"
	case TypeObject:
		return "object"
	}
	return "unknown"
}

// Kind return the type of the JavaScript value.  See the type Kind.
func (ctx *Context) Kind(v *Value) Kind {
	ret := C.JSValueGetType(ctx.ptr(), v.ptr())
	return Kind(ret)
}

// IsUndefined returns true if the value has type undefined.
func (ctx *Context) IsUndefined(v *Value) bool {
	ret := C.JSValueIsUndefined(ctx.ptr(), v.ptr())
	return bool(ret)
}

// IsNull returns true if the value has type null.
func (ctx *Context) IsNull(v *Value) bool {
	ret := C.JSValueIsNull(ctx.ptr(), v.ptr())
	return bool(ret)
}

// IsBoolean returns true if the value has type boolean.
func (ctx *Context) IsBoolean(v *Value) bool {
	ret := C.JSValueIsBoolean(ctx.ptr(), v.ptr())
	return bool(ret)
}

// IsNumber returns true if the value has type number.
func (ctx *Context) IsNumber(v *Value) bool {
	ret := C.JSValueIsNumber(ctx.ptr(), v.ptr())
	return bool(ret)
}

// IsString returns true if the value has type string.
func (ctx *Context) IsString(v *Value) bool {
	ret := C.JSValueIsString(ctx.ptr(), v.ptr())
	return bool(ret)
}

// IsObject returns true if the value has type object.
func (ctx *Context) IsObject(v *Value) bool {
	ret := C.JSValueIsObject(ctx.ptr(), v.ptr())
	return bool(ret)
}

func (ctx *Context) IsEqual(a *Value, b *Value) (eq bool, err *Value) {
	exception := C.JSValueRef(nil)

	ret := C.JSValueIsEqual(ctx.ptr(), a.ptr(), b.ptr(), &exception)
	if exception != nil {
		return false, toValue(exception)
	}

	return bool(ret), nil
}

func (ctx *Context) IsStrictEqual(a *Value, b *Value) bool {
	ret := C.JSValueIsStrictEqual(ctx.ptr(), a.ptr(), b.ptr())
	return bool(ret)
}

// NewUndefinedValue creates a new JavaScript value of the undefined type, 
// and returns a reference to the new value.
func (ctx *Context) NewUndefinedValue() *Value {
	ref := C.JSValueMakeUndefined(ctx.ptr())
	return toValue(ref)
}

// NewNullValue creates a new JavaScript value of the null type, 
// and returns a reference to the new value.
func (ctx *Context) NewNullValue() *Value {
	ref := C.JSValueMakeNull(ctx.ptr())
	return toValue(ref)
}

// NewBooleanValue creates a new JavaScript boolean value, 
// and returns a reference to the new value.
func (ctx *Context) NewBooleanValue(value bool) *Value {
	ref := C.JSValueMakeBoolean(ctx.ptr(), C.bool(value))
	return toValue(ref)
}

// NewNumberValue creates a new JavaScript number value, 
// and returns a reference to the new value.
func (ctx *Context) NewNumberValue(value float64) *Value {
	ref := C.JSValueMakeNumber(ctx.ptr(), C.double(value))
	return toValue(ref)
}

// NewString creates a new JavaScript string value, 
// and returns a reference to the new value.
func (ctx *Context) NewString(value string) *Value {
	cvalue := C.CString(value)
	defer C.free(unsafe.Pointer(cvalue))
	jsstr := C.JSStringCreateWithUTF8CString(cvalue)
	defer C.JSStringRelease(jsstr)
	ref := C.JSValueMakeString(ctx.ptr(), jsstr)
	return toValue(ref)
}

// ToBoolean returns the result of converting a JavaScript value to a boolean.
func (ctx *Context) ToBoolean(ref *Value) bool {
	ret := C.JSValueToBoolean(ctx.ptr(), ref.ptr())
	return bool(ret)
}

// ToNumber returns the result of converting a JavaScript value to a number.
// If the conversion succeeds, the value is returned in num.  If the conversion
// fails, the error raised by JavaScript is returned in err.
func (ctx *Context) ToNumber(ref *Value) (num float64, err *Value) {
	var exception C.JSValueRef
	ret := C.JSValueToNumber(ctx.ptr(), ref.ptr(), &exception)
	if exception != nil {
		return float64(ret), toValue(exception)
	}

	// Successful conversion
	return float64(ret), nil
}

// ToNumberOrDie returns the result of converting a JavaScript value to a number.
// If the conversion succeeds, the value is returned.  If the conversion
// fails, the function panics with error raised by JavaScript.
func (ctx *Context) ToNumberOrDie(ref *Value) float64 {
	ret, err := ctx.ToNumber(ref)
	if err != nil {
		panic(newPanicError(ctx, err))
	}
	return ret
}

// ToString returns the result of converting a JavaScript value to a string.
// If the conversion succeeds, the value is returned in num.  If the conversion
// fails, the error raised by JavaScript is returned in err.
func (ctx *Context) ToString(ref *Value) (str string, err *Value) {
	var exception C.JSValueRef
	ret := C.JSValueToStringCopy(ctx.ptr(), ref.ptr(), &exception)
	if exception != nil {
		// An error occurred
		// ret should be null
		return "", toValue(exception)
	}
	defer C.JSStringRelease(ret)

	// Successful conversion
	tmp := toString(ret)
	return tmp.String(), nil
}

// ToStringOrDie returns the result of converting a JavaScript value to a string.
// If the conversion succeeds, the value is returned.  If the conversion
// fails, the function panics with error raised by JavaScript.
func (ctx *Context) ToStringOrDie(ref *Value) string {
	str, err := ctx.ToString(ref)
	if err != nil {
		panic(newPanicError(ctx, err))
	}
	return str
}

// ToObject returns the result of converting a JavaScript value to an object.
// If the conversion succeeds, the value is returned in num.  If the conversion
// fails, the error raised by JavaScript is returned in err.
func (ctx *Context) ToObject(ref *Value) (obj *Object, err *Value) {
	var exception C.JSValueRef
	ret := C.JSValueToObject(ctx.ptr(), ref.ptr(), &exception)
	if exception != nil {
		// An error occurred
		// ret should be null
		return nil, toValue(exception)
	}

	// Successful conversion
	return toObject(ret), nil
}

// ToObjectOrDie returns the result of converting a JavaScript value to an object.
// If the conversion succeeds, the value is returned.  If the conversion
// fails, the function panics with error raised by JavaScript.
func (ctx *Context) ToObjectOrDie(ref *Value) *Object {
	ret, err := ctx.ToObject(ref)
	if err != nil {
		panic(newPanicError(ctx, err))
	}
	return ret
}

// ProtectValue prevents a value from being garbage collected.  In most cases,
// calling ProtectValue is unnecessary.  However, if you want to store the 
// Value for later use, then it must be protected.  Calls to ProtectValue 
// should be matched to calls to UnProtectValue when the Value is no longer
// needed.
func (ctx *Context) ProtectValue(ref *Value) {
	C.JSValueProtect(ctx.ptr(), ref.ptr())
}

// UnProtectValue undoes the protection from garbage collection.  All calls 
// should be matched with a preceeding call to ProtectValue.
func (ctx *Context) UnProtectValue(ref *Value) {
	C.JSValueProtect(ctx.ptr(), ref.ptr())
}
