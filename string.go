// Copyright 2011 Robert W Johnstone. All rights reserved.
// Use of this source code is governed by a BSD-style
// license that can be found in the LICENSE file.

package javascriptcore

import "syscall"
import "unsafe"

// #cgo pkg-config: webkit-1.0
// #include <stdlib.h>
// #include <JavaScriptCore/JSStringRef.h>
import "C"

// A String is a UTF-16 character buffer and is the native string representation
// in JavaScriptCore.  Most users should not need to use this package, as the
// other functions within the library handle the necessary conversions between
// Go strings and JavaScriptCore strings.
type String struct {
	private int // Internal structure is never accessed by Go.
}

// New creates a new string representation native to JavaScriptCore.  The newly
// created object has a reference count of one.  The caller should make a call
// to the member function Release when the string is no longer required to free
// the allocated memory.
func NewString(value string) *String {
	cvalue := C.CString(value)
	defer C.free(unsafe.Pointer(cvalue))
	ref := C.JSStringCreateWithUTF8CString(cvalue)
	return (*String)(unsafe.Pointer(ref))
}

// Retain increments the internal reference count of the JavaScriptCore string
// object.  To avoid a memory leak, all calls to retain should be followed by
// a matching call to the member function Release.
func (ref *String) Retain() {
	C.JSStringRetain(C.JSStringRef(unsafe.Pointer(ref)))
}

// Release decrements the internal reference count of the JavaScriptCore string
// object, and frees the memory when the count reaches zero.  See the member
// function Retain.
func (ref *String) Release() {
	C.JSStringRelease(C.JSStringRef(unsafe.Pointer(ref)))
}

// String returns a native Go string in UTF-8 that matches the internal UTF-16 representation.
func (ref *String) String() string {
	// Conversion 1, null-terminate UTF-8 string
	len := C.JSStringGetMaximumUTF8CStringSize(C.JSStringRef(unsafe.Pointer(ref)))
	buffer := C.malloc(len)
	if buffer == nil {
		panic(syscall.ENOMEM)
	}
	defer C.free(buffer)
	C.JSStringGetUTF8CString(C.JSStringRef(unsafe.Pointer(ref)), (*C.char)(buffer), len)

	// Conversion 2, Go string
	ret := C.GoString((*C.char)(buffer))
	return ret
}

// Length returns the number of Unicode characters in the string
func (ref *String) Length() uint32 {
	ret := C.JSStringGetLength(C.JSStringRef(unsafe.Pointer(ref)))
	return uint32(ret)
}

// Equal returns true if the two strings match. 
func (ref *String) Equal(rhs *String) bool {
	ret := C.JSStringIsEqual(C.JSStringRef(unsafe.Pointer(ref)), C.JSStringRef(unsafe.Pointer(rhs)))
	return bool(ret)
}

// Equal returns true if the two strings match, after converting the native Go string to UTF-16. 
func (ref *String) EqualToString(rhs string) bool {
	crhs := C.CString(rhs)
	defer C.free(unsafe.Pointer(crhs))
	ret := C.JSStringIsEqualToUTF8CString(C.JSStringRef(unsafe.Pointer(ref)), crhs)
	return bool(ret)
}
