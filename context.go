// Copyright 2011 Robert W Johnstone. All rights reserved.
// Use of this source code is governed by a BSD-style
// license that can be found in the LICENSE file.

package javascriptcore

// #include <stdlib.h>
// #include <JavaScriptCore/JSContextRef.h>
import "C"
import "unsafe"

// NewGlobalContext creates a new global JavaScript execution context.
// When the GlobalContext is no longer required, users should call Release.
func NewGlobalContext() *GlobalContext {
	const c_nil = unsafe.Pointer(uintptr(0))

	ctx := C.JSGlobalContextCreate((*[0]uint8)(c_nil))
	return (*GlobalContext)(unsafe.Pointer(ctx))
}

// Retain retains a GlobalContext.  Every call to Retain should be matched with a following call to Release.
func (ctx *GlobalContext) Retain() {
	C.JSGlobalContextRetain(ctx.ptr())
}

// Release realeases a GlobalContext.
func (ctx *GlobalContext) Release() {
	C.JSGlobalContextRelease(ctx.ptr())
}

// ToContext provides a conversion to *Context.  Since every GlobalContext is 
// a Context, this function does no work.  It  serves simply to minimize the 
// mismatch between Go's lack of support for inheritence.
func (ctx *GlobalContext) ToContext() *Context {
	return &ctx.Context
}

// GlobalObject returns the global object associated with an JavaScript execution context.
func (ctx *Context) GlobalObject() *Object {
	ret := C.JSContextGetGlobalObject(ctx.ptr())
	return toObject(ret)
}
