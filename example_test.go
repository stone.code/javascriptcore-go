// Copyright 2011 Robert W Johnstone. All rights reserved.
// Use of this source code is governed by a BSD-style
// license that can be found in the LICENSE file.

package javascriptcore

import (
	"fmt"
)

func ExampleNewString() {
	// Allocate a new JavaScriptCore string.
	str := NewString("a string")
	// Ensure that the memory is released when the function returns.
	defer str.Release()

	fmt.Println(str)
	// Output:
	// a string
}

func ExampleString_EqualToString() {
	str := NewString("a string")
	defer str.Release()

	if str.EqualToString("a different string") {
		fmt.Println("The two strings are equal.")
	} else {
		fmt.Println("The two strings are not equal.")
	}

	// Output:
	// The two strings are not equal.
}

func ExampleContext_EvaluateScript() {
	ctx := NewGlobalContext()
	defer ctx.Release()

	ret, err := ctx.EvaluateScript("function test() { return 1;}; test()", nil, "./fake.js", 1)
	if err != nil {
		fmt.Println("EvaluateScript raised an error (", err, ")")
	} else if ret == nil {
		fmt.Println("EvaluateScript failed to return a result")
	} else {
		stringval := ctx.ToStringOrDie(ret)
		fmt.Println("EvaluateScript returned a result: ", stringval)
	}

	// Output:
	// EvaluateScript returned a result:  1
}

func ExampleContext_NewFunction() {
	ctx := NewGlobalContext()
	defer ctx.Release()

	fn, err := ctx.NewFunction("myfun", []string{"a", "b"}, "return a+b;", "./testing.go", 1)
	if err != nil {
		fmt.Println("Failed to create function.")
	}
	fmt.Println("New value tests as a function?", ctx.IsFunction(fn) )

	// Output:
	// New value tests as a function? true
}

func ExampleContext_NewFunctionWithNative() {
	// Create the execution context
	ctx := NewGlobalContext()
	defer ctx.Release()

	// Wrap a callback and add it to the global object
	fn := func() {
		fmt.Println("Hello from Go from JavaScript!")
	}
	val := ctx.NewFunctionWithNative(fn)
	err := ctx.SetProperty(ctx.GlobalObject(), "gofunc", val.ToValue(), 0)
	if err != nil {
		panic(err)
	}

	// Run a script that will call back into Go.	
	_, err = ctx.EvaluateScript("gofunc()", nil, "./fake.js", 1)
	if err != nil {
		panic(err)
	}

	// Output:
	// Hello from Go from JavaScript!
}
