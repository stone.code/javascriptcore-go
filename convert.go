// Copyright 2011 Robert W Johnstone. All rights reserved.
// Use of this source code is governed by a BSD-style
// license that can be found in the LICENSE file.

package javascriptcore

// #include <JavaScriptCore/JSBase.h>
import "C"
import "unsafe"

func (ctx *Context) ptr() C.JSContextRef {
	return C.JSContextRef(unsafe.Pointer(ctx))
}

func (val *String) ptr() C.JSStringRef {
	return C.JSStringRef(unsafe.Pointer(val))
}

func (val *Value) ptr() C.JSValueRef {
	return C.JSValueRef(unsafe.Pointer(val))
}

func (o *Object) ptr() C.JSObjectRef {
	return C.JSObjectRef(unsafe.Pointer(o))
}

func toString(ptr C.JSStringRef) *String {
	return (*String)(unsafe.Pointer(ptr))
}

func toObject(ptr C.JSObjectRef) *Object {
	return (*Object)(unsafe.Pointer(ptr))
}

func toValue(ptr C.JSValueRef) *Value {
	return (*Value)(unsafe.Pointer(ptr))
}
