// Copyright 2011 Robert W Johnstone. All rights reserved.
// Use of this source code is governed by a BSD-style
// license that can be found in the LICENSE file.

package javascriptcore

// #cgo pkg-config: webkit-1.0
// #include <stdlib.h>
// #include <JavaScriptCore/JSBase.h>
import "C"

// A ContextGroup associates JavaScript Contexts with one another.  
// Objects may be shared between Contexts that belong to the same group.
type ContextGroup struct {
	private int // Internal structure is never accessed by Go.
}

// A Context holds the state and the global object for execution of JavaScript code.
type Context struct {
	private int // Internal structure is never accessed by Go.
}

// A GlobalContext is a global Context.
// A GlobalContext is a Context.
type GlobalContext struct {
	Context
}

// EvaluateScript evaluates a string of JavaScript.  If the script is well-formed,
// and executes without error, then the result of evaluating the JavaScript code
// will be returned.  Otherwise, an JavaScript error object will be returned.
func (ctx *Context) EvaluateScript(script string, thisObject *Object, source_url string, startingLineNumber int) (value *Value, error *Value) {
	scriptRef := NewString(script)
	defer scriptRef.Release()

	var sourceRef *String
	if source_url != "" {
		sourceRef = NewString(source_url)
		defer sourceRef.Release()
	}

	var exception C.JSValueRef

	ret := C.JSEvaluateScript(ctx.ptr(),
		scriptRef.ptr(), thisObject.ptr(),
		sourceRef.ptr(), C.int(startingLineNumber), &exception)
	if ret == nil {
		// An error occurred
		// Error information should be stored in exception
		return nil, toValue(exception)
	}

	// Successful evaluation
	return toValue(ret), nil
}

// CheckScriptSyntax checks for syntax errors in a string of JavaScript
func (ctx *Context) CheckScriptSyntax(script string, source_url string, startingLineNumber int) *Value {
	scriptRef := NewString(script)
	defer scriptRef.Release()

	var sourceRef *String
	if source_url != "" {
		sourceRef = NewString(source_url)
		defer sourceRef.Release()
	}

	var exception C.JSValueRef

	ret := C.JSCheckScriptSyntax(ctx.ptr(),
		scriptRef.ptr(), sourceRef.ptr(),
		C.int(startingLineNumber), &exception)
	if !ret {
		// A syntax error was found
		// exception should be non-nil
		return toValue(exception)
	}

	// exception should be nil
	return nil
}

// GarbageCollects initiates garbage collection of the values and objects in a context.
func (ctx *Context) GarbageCollect() {
	C.JSGarbageCollect(ctx.ptr())
}
