// Copyright 2011 Robert W Johnstone. All rights reserved.
// Use of this source code is governed by a BSD-style
// license that can be found in the LICENSE file.

/*
This package is a wrapper for JavaScriptCore, which is the JavaScript 
engine used by the WebKit web browser engine. In addition to providing 
access to the library, this wrapper allows callers to easily reflect 
native functions and objects from Go into the scripting environment.

For a complete description of the library, and the purpose and use of 
the JavaScriptCore API, please refer to the WebKit documentation.

As an example use of the library:

	func main() {
		ctx := js.NewContext()
		defer ctx.Release()

		ret, err := ctx.EvaluateScript("function test() { return 1;}; test()", nil, "./fake.js", 1)
		if err != nil {
			fmt.Println( "EvaluateScript raised an error (", err, ")" )
		} else if ret == nil {
			fmt.Println("EvaluateScript failed to return a result")
		} else {
			stringval := ctx.ToStringOrDie(ret)
			fmt.Println("EvaluateScript returned a result: ", stringval )
		}
	}

*/
package javascriptcore
