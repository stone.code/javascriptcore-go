// Copyright 2011 Robert W Johnstone. All rights reserved.
// Use of this source code is governed by a BSD-style
// license that can be found in the LICENSE file.

package javascriptcore

import (
	"testing"
)

type BaseTests struct {
	script string
	kind   Kind
	result string
}

var (
	basetests = []BaseTests{
		{"return 2341234 \"asdf\"", TypeUndefined, ""}, // syntax error
		{"1.5", TypeNumber, "1.5"},
		{"1.5 + 3.0", TypeNumber, "4.5"},
		{"'a' + 'b'", TypeString, "ab"},
		{"new Object()", TypeObject, "[object Object]"},
		{"var obj = {}; obj", TypeObject, "[object Object]"},
		{"var obj = function () { return 1;}; obj", TypeObject, "function () { return 1;}"},
		{"function test() { return 1;}; test", TypeObject, "function test() { return 1;}"},
		{"function test() { return 1;}; test()", TypeNumber, "1"}}
)

func TestBase(t *testing.T) {
	ctx := NewGlobalContext()
	defer ctx.Release()
}

func TestEvaluateScript(t *testing.T) {
	ctx := NewGlobalContext()
	defer ctx.Release()

	for index, item := range basetests {
		ret, err := ctx.EvaluateScript(item.script, nil, "./testing.go", 1)
		if item.result != "" {
			if err != nil {
				t.Errorf("ctx.EvaluateScript raised an error (script %v)", index)
			} else if ret == nil {
				t.Errorf("ctx.EvaluateScript failed to return a result (script %v)", index)
			} else {
				t.Logf("Type of value is %v\n", ctx.Kind(ret))
				kind := ctx.Kind(ret)
				if kind != item.kind {
					t.Errorf("ctx.EvaluateScript did not return the expected type (%v instead of %v).", kind, item.kind)
				}
				stringval := ctx.ToStringOrDie(ret)
				if stringval != item.result {
					t.Errorf("ctx.EvaluateScript returned an incorrect value (%v instead of %v).", stringval, item.result)
				}
			}
		} else {
			// Script has a syntax error
			if err == nil {
				t.Errorf("ctx.EvaluateScript did not raise an error on an invalid script")
			}
			if ret != nil {
				t.Errorf("ctx.EvaluateScript returned a result on an invalid script")
			}
		}
	}
}

func BenchmarkEvaluateScript(b *testing.B) {
	ctx := NewGlobalContext()
	defer ctx.Release()

	for index, item := range basetests {
		ret, err := ctx.EvaluateScript(item.script, nil, "./testing.go", 1)
		if item.result != "" {
			if err != nil {
				b.Errorf("ctx.EvaluateScript raised an error (script %v)", index)
			} else if ret == nil {
				b.Errorf("ctx.EvaluateScript failed to return a result (script %v)", index)
			} else {
				kind := ctx.Kind(ret)
				if kind != item.kind {
					b.Errorf("ctx.EvaluateScript did not return the expected type (%v instead of %v).", kind, item.kind)
				}
				stringval := ctx.ToStringOrDie(ret)
				if stringval != item.result {
					b.Errorf("ctx.EvaluateScript returned an incorrect value (%v instead of %v).", stringval, item.result)
				}
			}
		} else {
			// Script has a syntax error
			if err == nil {
				b.Errorf("ctx.EvaluateScript did not raise an error on an invalid script")
			}
			if ret != nil {
				b.Errorf("ctx.EvaluateScript returned a result on an invalid script")
			}
		}
	}
}

func TestCheckScript(t *testing.T) {
	ctx := NewGlobalContext()
	defer ctx.Release()

	for _, item := range basetests {
		err := ctx.CheckScriptSyntax(item.script, "./testing.go", 1)
		if err != nil && item.result != "" {
			t.Errorf("ctx.CheckScriptSyntax raised an error but script is good")
		}
		if err == nil && item.result == "" {
			t.Errorf("ctx.CheckScriptSyntax failed to raise an error but script is bad")
		}
	}
}

func TestGarbageCollect(t *testing.T) {
	ctx := NewGlobalContext()
	defer ctx.Release()

	ctx.GarbageCollect()
}
