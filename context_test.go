// Copyright 2011 Robert W Johnstone. All rights reserved.
// Use of this source code is governed by a BSD-style
// license that can be found in the LICENSE file.

package javascriptcore

import (
	"testing"
)

func TestContext(t *testing.T) {
	ctx := NewGlobalContext()
	defer ctx.Release()
}

func TestContext2(t *testing.T) {
	ctx := NewGlobalContext()
	defer ctx.Release()

	ctx.Retain()
	defer ctx.Release()
}

func TestContextGlobalObject(t *testing.T) {
	ctx := NewGlobalContext()
	defer ctx.Release()

	obj := ctx.GlobalObject()
	if obj == nil {
		t.Errorf("ctx.GlobalObject() returned nil")
	}
	if ctx.Kind(obj.ToValue()) != TypeObject {
		t.Errorf("ctx.GlobalObject() did not return a javascript object")
	}
}
